from distutils.core import setup

setup(
    name='pyBSDate',
    version='1.0.1',
    packages=['pyBSDate'],
    url='https://github.com/ayys/pyBSDate',
    license='GPL 3.0',
    author='ayys',
    author_email='ayushjha6@gmail.com',
    package_data={},
    description=''
)
